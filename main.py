import random, time

list_of_player_counter = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
list_of_computer_counter = ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k']

board = [
            [1, 'X', 2, 'X', 3, 'X', 4, 'X'],
            ['X', 5, 'X', 6, 'X', 7, 'X', 8],
            [9, 'X', 10, 'X', 11, 'X', 12, 'X'],
            ['X', 0, 'X', 0, 'X', 0, 'X', 0],
            [0, 'X', 0, 'X', 0, 'X', 0, 'X'],
            ['X', 'a', 'X', 'b', 'X', 'c', 'X', 'd'],
            ['e', 'X', 'f', 'X', 'g', 'X', 'h', 'X'],
            ['X', 'i', 'X', 'j', 'X', 'k', 'X', 'l']
        ]


def print_board():
    print("\n"*10)
    for n in range(0, 8):
        print(f"         {n}", end="")
    print('\n')

    for x in board:
        y = (board.index(x))
        print(f' {y}', end=' ')
        for k in x:
            print(f'    | {k} |', end=' ')
        print(f'\n')

    print(f'Lista pinków gracza: {list_of_player_counter}')
    print(f'Lista pionków komputera: {list_of_computer_counter}')

########################################################################################################################

class Player():

    def __init__(self):
        pass

    def move(self, counter_number, new_x, new_y):
        try:
            if board[new_x][new_y] == 0:
                for x in range(7,-1,-1):
                    for y in range(8):
                        if board[x][y] == counter_number:
                            pos = self.check_position(x, y, new_x, new_y, counter_number)
                            if pos == 0:
                                return 0
                            r_t = self.change_to_queen(counter_number, new_x)
                            if  r_t:
                                counter_number = r_t
                            board[new_x][new_y] = counter_number
                            board[x][y] = 0
                            print("OK")
                            return 1
            elif board[new_x][new_y] == "X":
                print("Niedozwolona pozycja ")
                return 0
            else:
                print("Pozycja zajęta")
                return 0
        except IndexError:
            print("Nie ma takiego pola")
            return 0



    def check_position(self, x, y, new_x, new_y, counter):
        r_x = new_x - x
        r_y = new_y - y

        if r_x == 1 and abs(r_y) == 1:
            return 1
        elif counter in [21, 22, 23, 24, 25, 26, 27, 28, 29, 210, 211, 212]:
            if abs(r_x) == abs(r_y):
                return 1
        # elif abs(r_x) == 2 and abs(r_y == 2):
        #     con = board[x + (r_x / 2)][y + (r_y / 2)]
        #     if con in list_of_computer_counter:
        #         list_of_computer_counter.remove(con)
        else:
            print("Za duży skok")
            return 0

    def check_bit(self):
        for counter_to_check in list_of_player_counter:
            for x in range(8):
                for y in range(8):
                    if board[x][y] == counter_to_check:
                        result = self.position_to_check(x, y, counter_to_check)
                        if result == 1:
                            return 1
        return 0

    def position_to_check(self, x, y, counter):
        list_of_position = [[x+1, y+1, x+2, y+2], [x+1, y-1, x+2, y-2], [x-1, y+1, x-2, y+2], [x-1, y-1, x-2, y-2]]

        for k, l, m, n in list_of_position:
            try:
                if m < 0 or n <0:
                    continue
                else:
                    if board[k][l] in list_of_computer_counter:
                        if board[m][n] == 0:
                            board[m][n] = counter
                            list_of_computer_counter.remove(board[k][l])
                            board[k][l] = 0
                            board[x][y] = 0
                            print("Obowiązek bicia")
                            time.sleep(2)
                            return 1
            except IndexError:
                continue

    def change_to_queen(self, counter_number, new_x):
        if new_x == 7 and counter_number not in [21, 22, 23, 24, 25, 26, 27, 28, 29, 210, 211, 212]:
            index = list_of_player_counter.index(int(counter_number))
            new_value = int('2' + str(counter_number))
            list_of_player_counter[index] = new_value
            return list_of_player_counter[index]
        return 0


########################################################################################################################

class Computer():
    def __init__(self):
        # self.counter = ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k']
        global list_of_computer_counter

    def move(self):
        result = 0
        while(True):
            counter = random.choice(list_of_computer_counter)
            for x in range(8):
                for y in range(8):
                    if board[x][y] == counter:
                        if len(board[x][y]) > 1:
                            while(True):
                                new_x = random.randint(0, 7)
                                res = new_x - x
                                if new_x != x:
                                    break
                            new_y = random.choice([y+res], [y-res])
                        else:
                            new_x = x - 1
                            new_y = random.choice([y-1, y+1])
                            # print(x, y)
                            # print(new_x, new_y)
                            r_t = self.change_to_queen(counter, new_x)
                            if r_t:
                                counter = r_t
                        result = self.check(new_x, new_y)
                        if result == 1:
                            board[new_x][new_y] = counter
                            board[x][y] = 0
                            break
            if result == 1:
                break

    def change_to_queen(self, counter, new_x):
        if new_x == 7 and counter not in ['aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh', 'ii', 'jj', 'kk', 'll']:
            index = list_of_computer_counter.index(counter)
            new_value = counter * 2
            list_of_computer_counter[index] = new_value
            return list_of_computer_counter[index]
        return 0


    def check(self, new_x, new_y):
        try:
            if board[new_x][new_y] == 0:
                return 1
            else:
                return 0
        except IndexError:
            print("Nie ma takiego pola")
            return 0

    def check_bit(self):
        for counter_to_check in list_of_computer_counter:
            for x in range(8):
                for y in range(8):
                    if board[x][y] == counter_to_check:
                        result = self.position_to_check(x, y, counter_to_check)
                        if result == 1:
                            return 1
        return 0

    def position_to_check(self, x, y, counter):
        list_of_position = [[x+1, y+1, x+2, y+2], [x+1, y-1, x+2, y-2], [x-1, y+1, x-2, y+2], [x-1, y-1, x-2, y-2]]

        for k, l, m, n in list_of_position:
            try:
                if m < 0 or n <0:
                    continue
                else:
                    if board[k][l] in list_of_player_counter:
                        if board[m][n] == 0:
                            board[m][n] = counter
                            list_of_player_counter.remove(board[k][l])
                            board[k][l] = 0
                            board[x][y] = 0
                            print("JEST JEST ZBITY!!!")
                            return 1
            except IndexError:
                continue








# def print_board():
#     print('\n'*10)
#     for row in board:
#         print(row)



# print_board()
player = Player()
computer = Computer()
# computer.move()
# print_board()

# player.move(1, 3, 1)
# print_board()

step = 1

while(True):

    print_board()
    print()

    if step % 2 == 0:
        while(True):
            if computer.check_bit() == 0:
                computer.move()
                break
        # computer.check()
        step +=1
    else:
        while(True):
            try:
                if player.check_bit() == 0:
                    player_counter = int(input("Podaj nazwę pionka: "))
                    x_position = int(input("Podaj X: "))
                    y_position = int(input("Podaj Y: "))
                    if player.move(player_counter, x_position, y_position) == 0:
                        continue
                break
            except ValueError:
                print("Błędna wartość")
                continue
        step += 1

    if list_of_computer_counter == []:
        print("Gracz wygrał")
        break
    elif list_of_player_counter == []:
        print("Komputer wygrał")
        break